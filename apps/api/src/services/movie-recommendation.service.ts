import { Movie } from '../models/movie';
import netflixService from './external-apis/netflix.service';
import imdbService from './external-apis/imdb.service';

/**
 * Get movie recommendations by genre
 * @param genre movie genre
 * @returns list of recommended movies by genre
 */
async function byGenre(genre: string): Promise<Movie[]> {
  try {
    const movies = await Promise.all([
      netflixService.recommendationsByGenre(genre),
      imdbService.recommendationsByGenre(genre),
    ]);
    return movies.flat();
  } catch (error) {
    console.error(error);
    throw new Error('Cannot fetch movie recommendations by genre from external APIs!');
  }
}

export default { byGenre };
