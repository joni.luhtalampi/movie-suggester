import { Movie } from '../../models/movie';

interface NetflixMovie {
  title: string;
  director: string;
  publishYear: number;
  coverImg: string;
}

/**
 * mock API return
 */
const mockMovies: NetflixMovie[] = [
  {
    title: 'Netflix Movie 1',
    director: 'Director 123',
    publishYear: 2010,
    coverImg: 'https://www.netflix.com/api/movies/covers/Movie1',
  },
  {
    title: 'Netflix Movie 12',
    director: 'Director 56',
    publishYear: 1998,
    coverImg: 'https://www.netflix.com/api/movies/covers/Movie12',
  },
];

/**
 * Fetch movie recommendations from external API
 * @param genre movie genre
 * @returns list of movie recommendations
 */
async function fetchRecommendationByGenre(genre: string): Promise<NetflixMovie[]> {
  // TODO: add async API call once the correct url is known
  const movies: NetflixMovie[] = mockMovies; // json-server could also be used for mock returns
  return movies;
}

/**
 * Get movie recommendations by genre
 * @param genre movie genre
 * @returns list of recommended movies by genre
 */
async function recommendationsByGenre(genre: string): Promise<Movie[]> {
  try {
    const movies = await fetchRecommendationByGenre(genre);
    // Process netflix movies for consistency
    const processedMovies = movies.map((movie) => {
      const { title, director, publishYear, coverImg } = movie;
      return {
        title,
        director,
        year: publishYear,
        imageUrl: coverImg,
      };
    });
    return processedMovies;
  } catch (error) {
    console.error(error);
    throw new Error('Cannot fetch movie recommendations by genre from Netflix!');
  }
}

export default { recommendationsByGenre };
