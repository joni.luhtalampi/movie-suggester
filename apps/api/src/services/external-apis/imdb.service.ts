import { Movie } from '../../models/movie';

interface ImdbMovie {
  title: string;
  director1: string;
  director2: string;
  releaseYear: number;
  imgUrl: string;
}

/**
 * mock API return
 */
const mockMovies: ImdbMovie[] = [
  {
    title: 'IMDB Movie 1',
    director1: 'Director1',
    director2: 'Director2',
    releaseYear: 2008,
    imgUrl: 'https://www.imdb.com/api/images/Movie1',
  },
  {
    title: 'IMDB Movie 2',
    director1: 'Director2',
    director2: 'Director3',
    releaseYear: 2019,
    imgUrl: 'https://www.imdb.com/api/images/Movie2',
  },
];

/**
 * Fetch movie recommendations from external API
 * @param genre movie genre
 * @returns list of movie recommendations
 */
async function fetchRecommendationByGenre(genre: string): Promise<ImdbMovie[]> {
  // TODO: add async API call once the correct url is known
  const movies: ImdbMovie[] = mockMovies; // json-server could also be used for mock returns
  return movies;
}

/**
 * Get movie recommendations by genre
 * @param genre movie genre
 * @returns list of recommended movies by genre
 */
async function recommendationsByGenre(genre: string): Promise<Movie[]> {
  try {
    const movies = await fetchRecommendationByGenre(genre);
    // Process imdb movies for consistency
    const processedMovies = movies.map((movie) => {
      const { title, director1, releaseYear, imgUrl } = movie;
      return {
        title,
        director: director1,
        year: releaseYear,
        imageUrl: imgUrl,
      };
    });
    return processedMovies;
  } catch (error) {
    console.error(error);
    throw new Error('Cannot fetch movie recommendations by genre from IMDB!');
  }
}

export default { recommendationsByGenre };
