export interface Movie {
  title: string;
  director: string;
  imageUrl: string;
  year: number;
  // etc...
}
