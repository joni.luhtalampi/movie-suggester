import express from 'express';
import movieRecommendationRouter from './routers/movie-recommendation.router';

const app = express();

app.use('/api/movies/recommendations/', movieRecommendationRouter);

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
