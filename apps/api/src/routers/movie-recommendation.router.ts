import express from 'express';
import movieRecommendationService from '../services/movie-recommendation.service';

const router = express.Router();

/**
 * Get movie recommendations by genre
 */
router.get('/genre/:genre', async (req, res) => {
  try {
    const movies = await movieRecommendationService.byGenre(req.params.genre);
    res.send(movies);
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: 'Internal server error!' });
  }
});

export default router;
